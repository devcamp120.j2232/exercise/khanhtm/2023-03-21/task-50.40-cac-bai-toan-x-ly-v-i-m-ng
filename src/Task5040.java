public class Task5040 {
    public static void main(String[] args) {
        //task1
        int x1 = 4;
        int y1 = 7;
        int[] arr1 = Task1(x1, y1);
        // In ra mảng
        System.out.print("[");
        for (int i = 0; i < arr1.length; i++) {
            System.out.print(arr1[i]);
            if (i != arr1.length-1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");  // [4, 5, 6, 7]

        int x2 = -4;
        int y2 = 7;
        int[] arr2 = Task1(x2, y2);
        // In ra mảng
        System.out.print("[");
        for (int i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i]);
            if (i != arr2.length-1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");  // [-4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7]

        System.out.println("--------------------------");
        //task2
        int[] arr21 = {1, 2, 3};
        int[] arr22 = {100, 2, 1, 10};
        int[] merged21 = mergeArrays(arr21, arr22);
        printArray(merged21);  // [1, 2, 3, 100, 10]

        int[] arr23 = {1, 2, 3};
        int[] arr24 = {1, 2, 3};
        int[] merged22 = mergeArrays(arr23, arr24);
        printArray(merged22);  // [1, 2, 3]

        System.out.println("--------------------------");

        //task3
        int[] arr31 = {1, 2, 1, 4, 5, 1, 1, 3, 1};
        int n31 = 1;
        int count31 = countOccurrences(arr31, n31);
        System.out.println("The number " + n31 + " appears " + count31 + " times in the array.");

        int[] arr32 = {1, 2, 1, 4, 5, 1, 1, 3, 1};
        int n32 = 2;
        int count32 = countOccurrences(arr32, n32);
        System.out.println("The number " + n32 + " appears " + count32 + " times in the array.");

        System.out.println("--------------------------");

        //task4
        int[] arr4 = {1, 2, 3, 4, 5, 6};
        int sum = sumArray(arr4);
        System.out.println("The sum of the elements in the array is: " + sum);

        System.out.println("--------------------------");

        //task5
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] evenArr = getEvenArray(arr);
        System.out.println("The even elements in the array are: " + toString(evenArr));

        System.out.println("--------------------------");

        //task6
        int[] arr61 = {1, 0, 2, 3, 4};
        int[] arr62 = {3, 5, 6, 7, 8, 13};
        int[] sumArr61 = getSumArray(arr61, arr62);
        System.out.println("The sum of the arrays is: " + arrayToString(sumArr61));

        int[] arr63 = {1, 2, 3};
        int[] arr64 = {1, 2, 3, 4, 5};
        int[] sumArr62 = getSumArray(arr63, arr64);
        System.out.println("The sum of the arrays is: " + arrayToString(sumArr62));

        System.out.println("--------------------------");

        //task7
        int[] arr7 = {1, 2, 3, 1, 5, 1, 4, 6, 3, 4};
        int[] uniqueArr = getUniqueArray(arr7);
        System.out.println("The unique elements of the array are: " + arrayToString7(uniqueArr));

        System.out.println("--------------------------");

        //task8
        int[] arr81 = {1, 2, 3};
        int[] arr82 = {100, 2, 1, 10};
        int[] uniqueArr81 = getUniqueElements(arr81, arr82);
        System.out.println("The unique elements of the arrays are: " + arrayToString8(uniqueArr81));

        int[] arr83 = {1, 2, 3};
        int[] arr84 = {1, 2, 3};
        int[] uniqueArr82 = getUniqueElements(arr83, arr84);
        System.out.println("The unique elements of the arrays are: " + arrayToString8(uniqueArr82));

        System.out.println("--------------------------");

        //task9
        int[] arr9 = {1, 3, 1, 4, 2, 5, 6};
        sortDescending(arr9);
        System.out.println("The sorted array is: " + arrayToString9(arr9));
        System.out.println("--------------------------");
        //task10
        int[] arr101 = {10, 20, 30, 40, 50};
        int x = 0, y = 2;
        System.out.println("The array before swapping elements is: " + arrayToString10(arr101));
        swapElements(arr101, x, y);
        System.out.println("The array after swapping elements at positions " + x + " and " + y + " is: " + arrayToString10(arr101));

        int[] arr102 = {10, 20, 30, 40, 50};
        x = 1;
        y = 2;
        System.out.println("The array before swapping elements is: " + arrayToString10(arr102));
        swapElements(arr102, x, y);
        System.out.println("The array after swapping elements at positions " + x + " and " + y + " is: " + arrayToString10(arr102));
    }

    public static int[] Task1(int x, int y) {
        int[] arr = new int[y-x+1];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = x+i;
        }
        return arr;
    }

    public static int[] mergeArrays(int[] arr1, int[] arr2) {
        int[] merged = new int[arr1.length + arr2.length];
        int i = 0;
        for (int num : arr1) {
            if (!contains(merged, num)) {
                merged[i++] = num;
            }
        }
        for (int num : arr2) {
            if (!contains(merged, num)) {
                merged[i++] = num;
            }
        }
        int[] result = new int[i];
        for (int j = 0; j < i; j++) {
            result[j] = merged[j];
        }
        return result;
    }

    public static boolean contains(int[] arr, int num) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == num) {
                return true;
            }
        }
        return false;
    }

    public static void printArray(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
            if (i != arr.length-1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }

    public static int countOccurrences(int[] arr, int n) {
        int count = 0;
        for (int num : arr) {
            if (num == n) {
                count++;
            }
        }
        return count;
    }

    public static int sumArray(int[] arr) {
        int sum = 0;
        for (int num : arr) {
            sum += num;
        }
        return sum;
    }

    public static int[] getEvenArray(int[] arr) {
        int[] evenArr = new int[arr.length];
        int index = 0;
        for (int num : arr) {
            if (num % 2 == 0) {
                evenArr[index++] = num;
            }
        }
        int[] result = new int[index];
        for (int i = 0; i < index; i++) {
            result[i] = evenArr[i];
        }
        return result;
    }

    public static String toString(int[] arr) {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < arr.length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(arr[i]);
        }
        sb.append("]");
        return sb.toString();
    }

    public static int[] getSumArray(int[] arr1, int[] arr2) {
        int length = Math.max(arr1.length, arr2.length);
        int[] sumArr = new int[length];
        for (int i = 0; i < length; i++) {
            int value1 = i < arr1.length ? arr1[i] : 0;
            int value2 = i < arr2.length ? arr2[i] : 0;
            sumArr[i] = value1 + value2;
        }
        return sumArr;
    }

    public static String arrayToString(int[] arr) {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < arr.length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(arr[i]);
        }
        sb.append("]");
        return sb.toString();
    }

    public static int[] getUniqueArray(int[] arr) {
        int[] tempArr = new int[arr.length];
        int tempIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            boolean isUnique = true;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    isUnique = false;
                    break;
                }
            }
            if (isUnique) {
                tempArr[tempIndex++] = arr[i];
            }
        }
        int[] uniqueArr = new int[tempIndex];
        System.arraycopy(tempArr, 0, uniqueArr, 0, tempIndex);
        return uniqueArr;
    }

    public static String arrayToString7(int[] arr) {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < arr.length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(arr[i]);
        }
        sb.append("]");
        return sb.toString();
    }

    public static int[] getUniqueElements(int[] arr1, int[] arr2) {
        int[] tempArr = new int[arr1.length + arr2.length];
        int tempIndex = 0;
        for (int i = 0; i < arr1.length; i++) {
            boolean isUnique = true;
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]) {
                    isUnique = false;
                    break;
                }
            }
            if (isUnique) {
                tempArr[tempIndex++] = arr1[i];
            }
        }
        for (int i = 0; i < arr2.length; i++) {
            boolean isUnique = true;
            for (int j = 0; j < arr1.length; j++) {
                if (arr2[i] == arr1[j]) {
                    isUnique = false;
                    break;
                }
            }
            if (isUnique) {
                tempArr[tempIndex++] = arr2[i];
            }
        }
        int[] uniqueArr = new int[tempIndex];
        System.arraycopy(tempArr, 0, uniqueArr, 0, tempIndex);
        return uniqueArr;
    }

    public static String arrayToString8(int[] arr) {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < arr.length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(arr[i]);
        }
        sb.append("]");
        return sb.toString();
    }

    public static void sortDescending(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] < arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    public static String arrayToString9(int[] arr) {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < arr.length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(arr[i]);
        }
        sb.append("]");
        return sb.toString();
    }

    public static void swapElements(int[] arr, int x, int y) {
        int temp = arr[x];
        arr[x] = arr[y];
        arr[y] = temp;
    }

    public static String arrayToString10(int[] arr) {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < arr.length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(arr[i]);
        }
        sb.append("]");
        return sb.toString();
    }

}